import React from 'react';
import { useUnit } from 'effector-react';
import { notifications } from '@mantine/notifications';
import { Button } from '@mantine/core';

import { $favorites } from '@/entities/favorites/model/store';
import { removeAllCoins } from '@/entities/favorites/model/events';

export const Actions = () => {
  const [deleteAllCoins, favs] = useUnit([removeAllCoins, $favorites]);

  const deleteFavoritesList = React.useCallback(() => {
    deleteAllCoins();
    notifications.show({
      id: `delete-favorites`,
      withCloseButton: false,
      autoClose: 2000,
      title: 'Избранные очищены',
      message: `Список избранных теперь пуст`,
      loading: false,
    });
  }, [deleteAllCoins]);

  if (!favs.length) {
    return <></>;
  }

  return <Button onClick={deleteFavoritesList}>Очистить избранное</Button>;
};
