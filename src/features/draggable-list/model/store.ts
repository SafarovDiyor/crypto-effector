import { createEvent, createStore } from 'effector';

export const $isDraggble = createStore(true);

export const changeIsDraggble = createEvent();

$isDraggble.on(changeIsDraggble, (state) => !state);
