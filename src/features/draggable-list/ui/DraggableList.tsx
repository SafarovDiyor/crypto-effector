'use client';
import { useWindowSize } from 'usehooks-ts';
import React from 'react';
import { motion } from 'framer-motion';
import { useUnit } from 'effector-react';

import { changeIsDraggble } from '../model/store';

import { CoinsList } from '@/entities/list';

export const DraggableList = () => {
  const { width, height } = useWindowSize();
  const [changeDraggblity] = useUnit([changeIsDraggble]);
  return (
    <motion.div
      dragConstraints={{ top: 65, left: 5, right: width - 100, bottom: height - 70 }}
      drag
      onDragStart={() => changeDraggblity()}
      onDragEnd={() => changeDraggblity()}
      whileDrag={{ scale: 1.03 }}
      whileTap={{ scale: 1.03 }}
    >
      <CoinsList />
    </motion.div>
  );
};
