import React from 'react';
import { Drawer as MantineDrawer } from '@mantine/core';

interface Props {
  opened: boolean;
  close: () => void;
}

export const Drawer = ({ opened, close, children }: React.PropsWithChildren<Props>) => {
  return (
    <MantineDrawer opened={opened} onClose={close} position="right" title="Избранное" radius={'md'} offset={8}>
      {children}
    </MantineDrawer>
  );
};
