import { createTheme, MantineProvider } from '@mantine/core';
import '@mantine/core/styles.css';

export const ThemeProvider = ({ children }: React.PropsWithChildren) => {
  const theme = createTheme({
    radius: {
      xs: '6px',
      sm: '8px',
      md: '10px',
      lg: '12px',
      xl: '14px',
    },
    colors: {
      grey: [
        "#f5f5f5",
        "#e7e7e7",
        "#cdcdcd",
        "#b2b2b2",
        "#9a9a9a",
        "#8b8b8b",
        "#848484",
        "#717171",
        "#656565",
        "#575757"
      ],
    },
    shadows: {
      md: '1px 1px 3px rgba(34, 60, 80, 0.6)',
      xl: '5px 5px 3px rgba(34, 60, 80, 0.6)',
    },
    headings: {
      sizes: {
        h1: { fontSize: '32px', fontWeight: '700' },
      },
    },
    fontSizes: {
      xs: '8px',
      sm: '14px',
      md: '16px',
      lg: '20px',
      xl: '24px',
    },
    primaryColor: 'grey',
    defaultRadius: 'md',
  });

  return (
    <MantineProvider theme={theme} defaultColorScheme="dark">
      {children}
    </MantineProvider>
  );
};
