'use client';
import React from 'react';
import { useUnit } from 'effector-react';
import { useDisclosure } from '@mantine/hooks';
import { Group } from '@mantine/core';

import { Header } from '@/widgets/header/ui/header';
import { FavoritesActions } from '@/features/favorites-actions';
import { Drawer } from '@/features/drawer';
import { DraggableList } from '@/features/draggable-list';
import { Favorites } from '@/entities/favorites/ui/favorites';
import { $favorites } from '@/entities/favorites/model/store';

const Home = () => {
  const [opened, { open, close }] = useDisclosure(false);

  const favorties = useUnit($favorites);

  React.useEffect(() => {
    console.log(favorties);
    if (opened && !favorties.length) {
      close();
    }
  }, [close, favorties, favorties.length, opened]);

  return (
    <React.Fragment>
      <Header opened={opened} open={open} />
      <Drawer opened={opened} close={close}>
        <FavoritesActions />
        <Favorites />
      </Drawer>
      <Group>
        <DraggableList />
      </Group>
    </React.Fragment>
  );
};

export default Home;
