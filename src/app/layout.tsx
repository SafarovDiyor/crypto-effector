import '@mantine/core/styles.css';
import Head from 'next/head';
import { Notifications } from '@mantine/notifications';
import { AppShell, ColorSchemeScript } from '@mantine/core';

import '@mantine/notifications/styles.css';
import { ThemeProvider } from '@/app/providers/ThemeProvider';
import { QueryProvider } from '@/app/providers/QueryProvider';

import '../../public/styles/globals.scss';

export default function RootLayout({ children }: React.PropsWithChildren) {
  return (
    <html lang="en">
      <Head>
        <ColorSchemeScript />
        <link rel="shortcut icon" href="/favicon.svg" />
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, user-scalable=no" />
      </Head>
      <body>
        <ThemeProvider>
          <Notifications position="top-right" limit={3} zIndex={1000} />
          <QueryProvider>
            <AppShell header={{ height: 60, offset: false }} zIndex={0} padding="md">
              {children}
            </AppShell>
          </QueryProvider>
        </ThemeProvider>
      </body>
    </html>
  );
}
