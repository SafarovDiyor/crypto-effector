import React from 'react';
import { useList, useUnit } from 'effector-react';
import { Flex } from '@mantine/core';

import { $favorites } from '../model/store';

import { Coin } from '@/widgets/list';

export const Favorites = () => {

  const favoritesList = useList($favorites, ({ name, coinId, img, price }, index) => (
    <Coin coinId={coinId} name={name} img={img} price={price} key={index} />
  ));

  return (
    <Flex mih={50} mt={16} gap="md" justify="center" align="flex-start" direction="column" wrap="wrap" miw={300}>
      {favoritesList}
    </Flex>
  );
};
