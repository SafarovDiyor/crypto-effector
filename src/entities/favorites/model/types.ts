export interface IFavoriteState {
  coins: ICoinModel[] | [];
}

export type ICoinModel = {
  price: string;
  img: string;
  change: string;
  name: string;
  bone: boolean;
  coinId: string;
};