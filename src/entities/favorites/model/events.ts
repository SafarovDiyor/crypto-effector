import { createEvent } from 'effector';

import { ICoinModel } from './types';

const addCoin = createEvent<ICoinModel>();
const removeCoin = createEvent<ICoinModel>();
const removeAllCoins = createEvent();

export { addCoin, removeCoin, removeAllCoins };
