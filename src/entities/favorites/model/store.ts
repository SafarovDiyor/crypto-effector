import { persist } from 'effector-storage/local';
import { createStore } from 'effector';

import { ICoinModel } from './types';
import { addCoin, removeAllCoins, removeCoin } from './events';

const initState: ICoinModel[] = [];

export const $favorites = createStore(initState, {
  name: 'favorites',
})
  .on(addCoin, (state, coin) => [...state, coin])
  .on(removeCoin, (state, coin) => state.filter((c) => c.coinId !== coin.coinId))
  .reset(removeAllCoins);

persist({
  store: $favorites,
  key: 'favorites',
});
