import { createEvent } from 'effector';

export const setScrollable = createEvent();
export const changeWidth = createEvent();
export const changeHeight = createEvent();
