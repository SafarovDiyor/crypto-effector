import { createStore } from 'effector';

import { changeHeight, changeWidth, setScrollable } from './events';

export const $scrollable = createStore(true).on(setScrollable, (_, type) => type);
export const $widthOffset = createStore(312).on(changeWidth, (_, width) => width);
export const $heightOffset = createStore(600).on(changeHeight, (_, height) => height);
