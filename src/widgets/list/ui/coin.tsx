import React from 'react';
import Image from 'next/image';
import { useUnit } from 'effector-react';
import { notifications } from '@mantine/notifications';
import { Card, HoverCard, Text, Notification, Transition } from '@mantine/core';

import { cutPrice } from '../lib/cutPrice';

import cs from './List.module.scss';

import { SkeletonVariant } from '@/shared/ui/skeletons/Skeleton';
import { Skeleton } from '@/shared/ui/skeletons';
import { CustomNotification } from '@/shared/ui/notification';
import { $favorites } from '@/entities/favorites/model/store';
import { addCoin } from '@/entities/favorites/model/events';
import { Coin as CoinInfo } from '@/entities/coin';

type Props = {
  price: string;
  img: string;
  change: string;
  name: string;
  bone: boolean;
  index: number;
  coinId: string;
};

export const Coin = ({ price, img, change, name, bone, coinId, index }: Partial<Props>) => {
  const [addToFavorites] = useUnit([addCoin]);

  const addCoinToFavorites = React.useCallback(() => {
    addToFavorites({ coinId, img, price, change, name, bone } as Props);
    notifications.show({
      id: `${coinId}-favorites`,
      withCloseButton: false,
      autoClose: 2000,
      title: 'Монета добавлена в избранное',
      message: `${name} теперь в списке избранных`,
      loading: false,
    });
  }, [addToFavorites, bone, change, coinId, img, name, price]);

  // if (bone) {
  //   return <Skeleton type={SkeletonVariant.COIN} />;
  // }

  return (
    <HoverCard shadow="md" closeDelay={400} withArrow closeOnClickOutside position="right">
      <HoverCard.Target>
        <Transition mounted={!bone} transition="fade" duration={(index as number) * 15 + 400} timingFunction="ease">
          {(styles) => (
            <Card style={styles} shadow="sm" miw={285} radius="lg" className={cs.card} onClick={addCoinToFavorites}>
              {img && <Image width={48} height={48} src={img!} alt={name!} />}
              <Text size={'sm'} mt={'sm'}>
                {name}
              </Text>
              <Text size={'xl'} fw={900} variant={'gradient'} gradient={{ from: '#e2e2e2', to: 'cyan', deg: 90 }}>
                $ {price && cutPrice(price as string)}
              </Text>
              <Text size="sm" fw={500} variant="gradient" gradient={{ from: 'white', to: 'black', deg: 120 }}>
                {change}
              </Text>
            </Card>
          )}
        </Transition>
      </HoverCard.Target>
      <HoverCard.Dropdown>{coinId && <CoinInfo coin={coinId as string} />}</HoverCard.Dropdown>
    </HoverCard>
  );
};
