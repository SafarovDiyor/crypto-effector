import React from 'react';
import { useUnit } from 'effector-react';
import { Container, Flex, ScrollArea } from '@mantine/core';

import { Coin } from './coin';

import { $isDraggble } from '@/features/draggable-list/model/store';
import { $heightOffset, $widthOffset } from '@/entities/list/model/stores';
import { ListI } from '@/entities/list/api/types';

type Props = {
  data: ListI | undefined;
  isLoading: boolean;
  isSuccess: boolean;
};

export const List = ({ data, isLoading, isSuccess }: Props) => {
  const [width, height, isDraggble] = useUnit([$widthOffset, $heightOffset, $isDraggble]);

  const Content = () => {
    return (
      <Flex gap="md" direction="column" wrap="wrap">
        {isSuccess &&
          data?.result.map(({ id, icon, name, symbol, price }, index) => {
            return (
              <Coin
                key={id}
                index={index}
                price={String(price)}
                name={name}
                img={icon}
                change={symbol}
                bone={false}
                coinId={id}
              />
            );
          })}
        {isLoading && Array.from({ length: 10 }).map((_, index) => <Coin key={index} bone={true} />)}
      </Flex>
    );
  };

  return isDraggble ? (
    <Container h={height} w={width} p={0}>
      <ScrollArea h={height + 20} scrollbarSize={3} p={0}>
        {Content()}
      </ScrollArea>
    </Container>
  ) : (
    <Container h={height} w={width} p={0}>
      {Content()}
    </Container>
  );
};
