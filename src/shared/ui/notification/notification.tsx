import React from 'react';
import { Notification, Transition } from '@mantine/core';

import cs from './notification.module.scss';

type Props = {
  isVisible: boolean;
  text: string;
};

export const CustomNotification = ({ isVisible, text }: Props) => {
  return (
    <Transition mounted={isVisible} transition="fade" duration={500} timingFunction="ease">
      {(styles) => (
        <div style={styles}>
          <Notification title="Монета добавлена в избранное!" withCloseButton={false}>
            {text}
          </Notification>
        </div>
      )}
    </Transition>
  );
};
